<?php

namespace admin;

class Controller_Login extends \Controller_Template
{

    public $template = 'admin';

    /**
     * before
     *
     * @return void
     */
    public function before()
    {
        parent::before();
    }

    /**
     * action_index
     *
     * @return void
     */
    public function action_index()
    {
        $this->template->content = \View::forge('login/index');

        if ($_POST)
        {
            $username = \Input::post('email');
            $password = \Input::post('password');
            $status = 1; // 1: visible, 0: invisible
            
            $user = \Auth::login($username, $password, $status);
            if ($user == false)
            {
                // login failed
                \Session::set_flash('error', "Login failed");
                \Response::redirect('admin/login/index');
            }
            \Response::redirect('admin/dashboard');
        }
    }

    /**
     * action_temp - create user admin, private url
     *
     * @return void
     */
    public function action_temp()
    {
        $username = "tuananh";
        $password = "!Gaster59";
        $email = "trantuananh198610@gmail.com";
        $group = 1;
        $profile = [];
        \Auth::create_user($username, $password, $email, $group, $profile);
    }

    /**
     * after
     *
     * @param  mixed $response
     *
     * @return void
     */
    public function after($response)
    {
        $response = parent::after($response); 
        return $response; // make sure after() returns the response object
    }

}
