<?php

namespace admin;

class Controller_Dashboard extends Controller_Base
{

    /**
     * before
     *
     * @return void
     */
    public function before()
    {
        parent::before();
    }

    
    /**
     * action_index
     *
     * @return void
     */
    public function action_index()
    {
        $this->template->content = \View::forge('dashboard/index');
    }

    /**
     * after
     *
     * @param  mixed $response
     *
     * @return void
     */
    public function after($response)
    {
        $response = parent::after($response); 
        return $response; // make sure after() returns the response object
    }

}