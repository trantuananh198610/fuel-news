<?php

namespace admin;

use admin\repository;
use admin\services;
use Intervention\Image\ImageManager;

class Controller_Post extends Controller_Base
{

    var $genres = [];
    var $thumb = 'thumb100100';

    /**
     * before
     *
     * @return void
     */
    public function before()
    {
        parent::before();

        $data = repository\Genre::getAllGenres();
        $data = $data->as_array();
        $genres = [];
        foreach($data as $item)
        {
            $genres[$item['id']] = $item['title'];
        }
        $this->genres = $genres;
    }

    public function action_index()
    {
        echo '2323';die;
        $this->template->content = \View::forge('post/index');
    }

    public function action_add()
    {
        $this->template->content = \View::forge('post/add', [
            'genres' => $this->genres
        ]);

        if ($_POST)
        {
            // $this->verifyToken();

            $validate = repository\Post::preparedValidate(array_keys($this->genres));
            $resultUpload = services\util::uploadImage();
            $errorUpload = '';
            $imgUpload = '';
            if (isset($resultUpload['error'])) 
            {
                $errorUpload = $resultUpload['error'];
            }
            else
            {
                $imgUpload = $resultUpload['img'];
            }
            if ($validate->run())
            {
                if (!is_null($errorUpload))
                {
                    $this->template->content = \View::forge('post/add', [
                        'errorUpload' => $errorUpload
                    ]);    
                }
                
                $data_post = $this->setDataPost($imgUpload);
                $genre = repository\Post::insert($data_post);

                if (!empty($imgUpload))
                {
                    $this->setThumbImage(($imgUpload));
                }
                
                \Response::redirect('admin/post');
            }
            else
            {
                $this->template->content = \View::forge('post/add', [
                    'validate' => $validate,
                    'errorUpload' => $errorUpload,
                    'genres' => $this->genres
                ]);
            }
        }
    }

    /**
     * action_edit
     *
     * @param  int $id
     *
     * @return void
     */
    public function action_edit($id)
    {
        $post = repository\Post::getOnePostById($id);
        if (empty(count($post))) {
            \Response::redirect('admin/post');
        }
        $this->template->content = \View::forge('post/edit', [
            'genres' => $this->genres,
            'post' => $post[0]
        ]);

        if ($_POST)
        {
            $validate = repository\Post::preparedValidate(array_keys($this->genres));
            $resultUpload = services\util::uploadImage();
            $errorUpload = '';
            $imgUpload = '';
            if (isset($resultUpload['error'])) 
            {
                $errorUpload = $resultUpload['error'];
            }
            else
            {
                $imgUpload = $resultUpload['img'];
            }
            if ($validate->run())
            {
                if (!is_null($errorUpload))
                {
                    $this->template->content = \View::forge('post/add', [
                        'errorUpload' => $errorUpload
                    ]);    
                }
                
                $data_post = $this->setDataPost($imgUpload, 'edit');
                $genre = repository\Post::update($id, $data_post);

                if (!empty($imgUpload))
                {
                    $this->setThumbImage(($imgUpload));
                }

                \Response::redirect('admin/post');
            }
            else
            {
                $this->template->content = \View::forge('post/edit', [
                    'validate' => $validate,
                    'errorUpload' => $errorUpload,
                    'genres' => $this->genres
                ]);
            }
        }
    }

    /**
     * verifyToken
     *
     * @return void
     */
    private function verifyToken()
    {
        if (! \Security::check_token())
        {
            // Wrong token
            echo 'Wrong token';
            die;
        }
    }

    /**
     * setDataPost
     *
     * @param  string $imgUpload
     *
     * @return array
     */
    private function setDataPost($imgUpload, $method = 'add')
    {
        $now = date('Y-m-d H:i:s');
        $data_post = [
            'name' => \Input::post('name'),
            'description' => \Input::post('description'),
            'content' => \Input::post('content'),
            'genre_id' => \Input::post('genre_id'),
            'img' => $imgUpload,
            'meta_tag' => \Input::post('meta_tag'),
            'meta_description' => \Input::post('meta_description'),
            'status' => 1, // 1: visible, 0: invisible
            'updated_at' => $now
        ];
        if ($method == 'add')
        {
            $data_post['created_at'] = $now;
        }
        return $data_post;
    }

    /**
     * setThumbImage - save thumbnail image
     *
     * @param  string $imgUpload
     *
     * @return void
     */
    private function setThumbImage($imgUpload)
    {
        $manager = new ImageManager(['driver'=>'gd']);
        $originPathImage = DOCROOT.'assets'.DS.'posts'.DS.$imgUpload;
        $folderThumb = DOCROOT.'assets'.DS.'posts'.DS.$this->thumb;
        
        // check exists folder
        $isExistsDir = true;
        try 
        {
            $readFolderThumb = !\File::read_dir($folderThumb);
        }
        catch(\Exception $ex)
        {
            $isExistsDir = false;
        }
        
        if (!$isExistsDir)
        {
            \File::create_dir(DOCROOT, 'assets'.DS.'posts'.DS.$this->thumb, 0755);
        }
        $newPathImage = DOCROOT.'assets'.DS.'posts'.DS.$this->thumb.DS.$imgUpload;
        $manager->make($originPathImage)->resize(100,100)->save($newPathImage);
    }

    /**
     * after
     *
     * @param  mixed $response
     *
     * @return void
     */
    public function after($response)
    {
        $response = parent::after($response); 
        return $response; // make sure after() returns the response object
    }

}