<?php

namespace admin;

class Controller_Base extends \Controller_Template
{
    /**
     * before
     *
     * @return void
     */
    public function before()
    {
        parent::before();

        if (!\Auth::check())
        {
            // Not login yet
            \Session::set_flash('error', "Please login");
            \Response::redirect('admin/login/index');
        }
        $group = \Auth::get('group');
        if ($group != 1)
        {
            \Session::set_flash('error', "No authorization");
            \Response::redirect('admin/login/index');
        }

        $this->template->navbar = \View::forge('_partials/navbar');
        $this->template->sidebar = \View::forge('_partials/sidebar');
        $this->template->title = "Admin page";
    }

    /**
     * after
     *
     * @param  mixed $response
     *
     * @return void
     */
    public function after($response)
    {
        $response = parent::after($response);
        return $response; // make sure after() returns the response object
    }
}
