<?php

namespace admin;

use admin\repository;

class Controller_Genre extends Controller_Base
{
    
    /**
     * before
     *
     * @return void
     */
    public function before()
    {
        parent::before();
    }

    /**
     * action_index
     *
     * @return void
     */
    public function action_index()
    {
        $data = repository\Genre::getGenres();
        $this->template->content = \View::forge('genre/index', [
            'genres' => $data['genres'],
            'pagination' => $data['pagination'],
        ]);
    }

    /**
     * action_add
     *
     * @return void
     */
    public function action_add()
    {
        $this->template->content = \View::forge('genre/add');
        if ($_POST)
        {
            if (! \Security::check_token())
            {
                // Wrong token
                echo 'Wrong token';
                die;
            }

            $now = date('Y-m-d H:i:s');
            $data_genre = [
                'title' => \Input::post('title'),
                'body' => \Input::post('body'),
                'meta_tag' => \Input::post('meta_tag'),
                'meta_description' => \Input::post('meta_description'),
                'status' => 1, // 1: visible, 0: invisible
                'created_at' => $now,
                'updated_at' => $now
            ];
            $validate = repository\Genre::preparedValidate();
            if ($validate->run())
            {
                $genre = repository\Genre::insert($data_genre);
                \Response::redirect('admin/genre');
            }
            else
            {
                $this->template->content = \View::forge('genre/add', [
                    'validate' => $validate
                ]);
            }
        }
    }

    /**
     * action_edit
     *
     * @param  integer $id
     *
     * @return void
     */
    public function action_edit($id)
    {
        $genre = repository\Genre::getOneGenreById($id);
        if (empty(count($genre))) {
            \Response::redirect('admin/genre');
        }
        $this->template->content = \View::forge('genre/edit', [
            'genre' => $genre[0]
        ]);

        if ($_POST)
        {
            if (! \Security::check_token())
            {
                // Wrong token
                echo 'Wrong token';
                die;
            }

            $now = date('Y-m-d H:i:s');
            $data_genre = [
                'title' => \Input::post('title'),
                'body' => \Input::post('body'),
                'meta_tag' => \Input::post('meta_tag'),
                'meta_description' => \Input::post('meta_description'),
                'status' => 1, // 1: visible, 0: invisible
                'updated_at' => $now
            ];
            $validate = repository\Genre::preparedValidate();
            if ($validate->run())
            {
                // echo '2323';die;
                $genre = repository\Genre::update($id, $data_genre);
                \Response::redirect('admin/genre');
            }
            else
            {
                $this->template->content = \View::forge('genre/add', [
                    'validate' => $validate
                ]);
            }
        }
    }

    public function action_delete($id)
    {
        $genre = repository\Genre::getOneGenreById($id);
        if (empty(count($genre))) {
            \Session::set_flash('not_found', "$id Not Found");
            \Response::redirect('admin/genre');
        }
        repository\Genre::delete($id);
        \Response::redirect('admin/genre');
    }

    /**
     * after
     *
     * @param  mixed $response
     *
     * @return void
     */
    public function after($response)
    {
        $response = parent::after($response); 
        return $response; // make sure after() returns the response object
    }
}
