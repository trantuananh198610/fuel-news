<?php

namespace admin\repository;

class Post
{
    public static $_table_name = 'posts';

	/**
	 * insert
	 *
	 * @param  array $data
	 *
	 * @return boolean
	 */
	public static function insert($data)
	{
		try
		{
			\DB::insert(self::$_table_name)->set($data)->execute();
			return true;
		}
		catch(\Exception $ex)
		{
			return false;
		}
	}

	/**
	 * update
	 *
	 * @param  int $id
	 * @param  array $data
	 *
	 * @return boolean
	 */
	public static function update($id, $data)
	{
		try
		{
			\DB::update(self::$_table_name)->set($data)->where('id', '=',$id)->execute();
			return true;
		}
		catch(\Exception $ex)
		{
			return false;
		}
	}

	/**
	 * delete - change status 1 to 0
	 *
	 * @param  int $id
	 *
	 * @return boolean
	 */
	public static function delete($id)
	{
		try
		{
			\DB::update(self::$_table_name)->set(['status' => 0])->where('id', '=',$id)->execute();
			return true;
		}
		catch(\Exception $ex)
		{
			return false;
		}
	}

	/**
	 * preparedValidate
	 *
	 * @return Validation
	 */
	public static function preparedValidate($genres)
	{
		$val = \Validation::forge();

		$val->set_message('required', 'Bạn chưa nhập :label');
		$val->set_message('min_length', ':label cần ít nhất :param:1 ký tự');
		$val->set_message('max_length', ':label chỉ tối đa :param:1 ký tự');

		// \Lang::load('post');
		$val->add('name', 'Name')->add_rule('required')
									->add_rule('min_length', 3)
                                    ->add_rule('max_length', 50);
        $val->add('description', 'Description')->add_rule('max_length', 250);
        $val->add('genre_id')->add_rule('match_collection', $genres);
		return $val;
	}

	/**
	 * getPosts
	 *
	 * @return array 
	 */
	public static function getPosts()
	{
		$data = \DB::select('id', 'title')
					->from(self::$_table_name)
					->where('status', 1)
					->execute();

		$config = [
			'pagination_url' => '/admin/genre',
			'total_items' => count($data),
			'per_page' => 10,
			'uri_segment' => 'page',
			'name' => 'bootstrap3-option' // set template pagination
		];

		$pagination = \Pagination::forge('my-pagination', $config);

		$data = \DB::select('id', 'title')
					->from(self::$_table_name)
					->where('status', 1)
					->order_by('created_at', 'desc')
					->limit($pagination->per_page)
					->offset($pagination->offset)
					->execute();
		return [
			'genres' => $data, 
			'pagination' => $pagination];
	}

	/**
	 * getOnePostById
	 *
	 * @param  int $id
	 *
	 * @return array
	 */
	public static function getOnePostById($id)
	{
		$data = \DB::select('id', 'name', 'description', 'content', 'genre_id', 'img', 'meta_tag', 'meta_description')
					->from(self::$_table_name)
					->where('status', 1)
					->where('id', $id)
					->execute();
		return $data;
	}

}
