<?php

namespace admin\services;

class Util
{
    public static function text()
    {
        echo '111';die;
    }

    public static function uploadImage()
    {
        $config = [
            'path' => DOCROOT.'assets'.DS.'posts',
            'randomize' => true,
            'ext_whitelist' => array('img', 'jpg', 'jpeg', 'gif', 'png'),
        ];
        \Upload::process($config);
        $result = [];

        
        if (empty(\Input::file('avatar')['name'])) {
            $result['img'] = '';
            return $result;
        }

        if (\Upload::is_valid())
        {
            // save image success
            \Upload::save();
            $avatar = \Upload::get_files('avatar');            
            $result['img'] = $avatar['saved_as'];
        }

        // If has errors
        if (\Upload::get_errors('avatar'))
        {
            $avatar = \Upload::get_errors('avatar');
            $errorAvatar = $avatar['errors'];
            $result['error'] = $errorAvatar[0]['message'];
        }
        return $result;
    }

}
