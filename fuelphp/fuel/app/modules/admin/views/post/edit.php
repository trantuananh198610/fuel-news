<script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo \Uri::create('admin/genre/index') ?>">Genre</a></li>
            <li class="active">Post - Edit</li>
        </ol>
    </div><!--/.row-->
    
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Post - Edit</h1>
        </div>
    </div><!--/.row-->
            
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Post - Edit</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <?php echo \Form::open([
                            'method' => 'post',
                            'class' => 'form-horizontal',
                            'enctype' => 'multipart/form-data'
                        ]); ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Name</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::input('name', isset($validate) ? $validate->input('name') : $post['name'], ['class' => 'form-control']);
                                    
                                        if ( isset($validate) && $validate->error('name') != null) {
                                            ?>
                                            <div class="alert alert-danger" role="alert">
                                            <?php echo $validate->error('name'); ?>
                                            </div>
                                            <?php
                                        } 
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Description</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::textarea('description', isset($validate) ? $validate->input('description') : $post['description'], ['class' => 'form-control']);

                                        if ( isset($validate) && $validate->error('description') != null) {
                                            ?>
                                            <div class="alert alert-danger" role="alert">
                                            <?php echo $validate->error('description'); ?>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Content</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::textarea('content', isset($validate) ? $validate->input('content') : $post['content'], ['class' => 'form-control'])
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Genre</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::select('genre_id', $post['genre_id'], $genres);

                                        if ( isset($validate) && $validate->error('genre_id') != null) {
                                            ?>
                                            <div class="alert alert-danger" role="alert">
                                            <?php echo $validate->error('genre_id'); ?>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Avatar</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::file('avatar');

                                        if (isset($errorUpload) && !empty($errorUpload)) {
                                            ?>
                                            <div class="alert alert-danger" role="alert">
                                            <?php echo $errorUpload; ?>
                                            </div>
                                            <?php
                                        }
                                    ?>
                                    <br />
                                    <?php echo \Asset::img('assets/posts/thumb100100/'.$post['img']) ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta tag</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::input('meta_tag', isset($validate) ? $validate->input('meta_tag') : $post['meta_tag'], ['class' => 'form-control'])
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta description</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::textarea('meta_description', isset($validate) ? $validate->input('meta_description') : $post['meta_description'], ['class' => 'form-control'])
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Edit</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div>
                            <?php echo \Form::csrf(); ?>
                        <?php echo \Form::close(); ?>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->
    
</div><!--/.main-->

<script type="text/javascript">

    // CKEDITOR.replace('body', {
    //     height: 200,
    //     // filebrowserImageBrowseUrl: '/apps/ckfinder/3.4.5/ckfinder.html?type=Images',
    //     filebrowserImageBrowseUrl: '<?php echo \Uri::create("admin/upload/index")."?type=Images"; ?>',
    // });

</script>