<div class="row">
    <div
        class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">Log in</div>
            <div class="panel-body">
                <?php echo \Form::open([
                    'method' => 'post',
                    'role' => 'form'
                ]); ?>
                    <fieldset>
                        <?php
                            if (\Session::get_flash('error')) {
                        ?>
                        <div class="alert alert-danger" role="alert">
                        <?php
                                echo \Session::get_flash('error');          
                            }
                        ?>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="E-mail" name="email" type="text" autofocus="">
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password"
                                name="password" type="password" value="">
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                        <?php echo \Form::csrf(); ?>
                    </fieldset>
                <?php echo \Form::close(); ?>
            </div>
        </div>
    </div>
    <!-- /.col-->
</div>
<!-- /.row -->