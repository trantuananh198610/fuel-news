<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo $title; ?></title>

        <?php echo Asset::css("bootstrap.min.css") ?>
        <?php echo Asset::css("datepicker3.css") ?>
        <?php echo Asset::css("styles.css") ?>

        <!--Icons-->
        <?php echo Asset::js("lumino.glyphs.js") ?>

    </head>
    <body>
        <?php echo $navbar; ?>

        <?php echo $sidebar; ?>

        <?php echo $content; ?>


        <?php echo Asset::js("jquery-1.11.1.min.js") ?>
        <?php echo Asset::js("bootstrap.min.js") ?>
        <?php echo Asset::js("chart.min.js") ?>
        <?php echo Asset::js("chart-data.js") ?>
        <?php echo Asset::js("easypiechart.js") ?>
        <?php echo Asset::js("easypiechart-data.js") ?>
        <?php echo Asset::js("bootstrap-datepicker.js") ?>
        <script>
            $('#calendar').datepicker({
            });

            !function ($) {
                $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
                    $(this).find('em:first').toggleClass("glyphicon-minus");      
                }); 
                $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
            }(window.jQuery);

            $(window).on('resize', function () {
            if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
            })
            $(window).on('resize', function () {
            if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
            })
        </script>	

    </body>
</html>