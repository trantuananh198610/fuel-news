<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a>
            </li>
            <li class="active">Genres</li>
        </ol>
    </div><!--/.row-->
    
    <div class="row">
        <div class="col-lg-9">
            <h1 class="page-header">List Genres</h1>
        </div>
        <div class="col-lg-3" style="margin-top: 40px;">
            <a href="<?php echo \Uri::create('admin/genre/add') ?>" class="btn btn-primary">Create</a>
        </div>
    </div><!--/.row-->

    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <?php
                if (\Session::get_flash("not_found")) {
                    ?>
                    <div class="alert alert-danger"><?php echo \Session::get_flash("not_found"); ?></div>
                    <?php
                }
                ?>
                
                <div class="panel-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td>Title</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach($genres as $genre)
                                {
                                    ?>
                                    <tr>
                                        <td><?php echo $genre['title'] ?></td>
                                        <td style="width: 15%;">
                                            <a href="<?php echo \Uri::create('admin/genre/edit/'.$genre['id']) ?>">Edit</a> | 
                                            <a href="<?php echo \Uri::create('admin/genre/delete/'.$genre['id']) ?>">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <?php echo $pagination->render(); ?>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div><!--/.row-->

</div>	<!--/.main-->