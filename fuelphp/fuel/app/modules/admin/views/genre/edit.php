<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
    <div class="row">
        <ol class="breadcrumb">
            <li><a href="<?php echo \Uri::create('admin/dashboard') ?>"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
            <li><a href="<?php echo \Uri::create('admin/genre/index') ?>">Genre</a></li>
            <li class="active">Genre - Edit</li>
        </ol>
    </div><!--/.row-->
    
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Genre - Edit</h1>
        </div>
    </div><!--/.row-->
            
    
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Genre - Edit</div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <?php echo \Form::open([
                            'method' => 'post',
                            'class' => 'form-horizontal'
                        ]); ?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::input('title', isset($validate) ? $validate->input('title') : $genre['title'], ['class' => 'form-control'])
                                    ?>
                                    <?php
                                        if ( isset($validate) && $validate->error('title') != null) {
                                            ?>
                                            <div class="alert alert-danger" role="alert">
                                            <?php echo $validate->error('title'); ?>
                                            </div>
                                            <?php
                                        }
                                        
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Body</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::textarea('body', isset($validate) ? $validate->input('body') : $genre['body'], ['class' => 'form-control'])
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta tag</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::input('meta_tag', isset($validate) ? $validate->input('meta_tag') : $genre['meta_tag'], ['class' => 'form-control'])
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Meta description</label>
                                <div class="col-sm-10">
                                    <?php
                                        echo \Form::textarea('meta_description', isset($validate) ? $validate->input('meta_description') : $genre['meta_description'], ['class' => 'form-control'])
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    <button type="reset" class="btn btn-default">Reset</button>
                                </div>
                            </div>
                            <?php echo \Form::csrf(); ?>
                        <?php echo \Form::close(); ?>
                </div>
            </div>
        </div><!-- /.col-->
    </div><!-- /.row -->
    
</div><!--/.main-->