<?php

namespace Fuel\Migrations;

class Create_genres
{
	public function up()
	{
		\DBUtil::create_table('genres', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
            'title' => array('constraint' => 50, 'type' => 'varchar'),
			'body' => array('type' => 'text'),
			'meta_tag' => array('type' => 'text'),
			'meta_description' => array('type' => 'text'),
			'status' => array('type' => 'int'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime')
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('genres');
	}
}
