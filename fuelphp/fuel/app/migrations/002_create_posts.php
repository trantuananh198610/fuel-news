<?php

namespace Fuel\Migrations;

class Create_posts
{
	public function up()
	{
		\DBUtil::create_table('posts', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'name' => array('constraint' => 50, 'type' => 'varchar'),
			'description' => array('constraint' => 250, 'type' => 'varchar'),
			'content' => array('type' => 'text'),
			'genre_id' => array('constraint' => 11, 'type' => 'int'),
			'img' => array('constraint' => 250, 'type' => 'varchar'),
			'meta_tag' => array('type' => 'text'),
			'meta_description' => array('type' => 'text'),
			'status' => array('type' => 'int'),
			'created_at' => array('type' => 'datetime'),
			'updated_at' => array('type' => 'datetime')
		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('posts');
	}
}
