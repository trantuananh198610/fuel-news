<?php

class Model_Genre extends \Model_Crud
{
	protected static $_properties = array(
		"id",
		'title',
		'body',
		'meta_tag',
		'meta_description',
		'meta_description',
		'status',
		'created_at',
		'updated_at'
	);

	protected static $_table_name = 'genres';

}
