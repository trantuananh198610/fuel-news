Cach chay ung dung:

1. Mở màn hình terminal, đi tới thư mục chứa project
2. chạy lệnh docker-compose up -d, để start project
3. chạy lệnh docker-compose exec app bash để truy cập vào terminal riêng của ứng dụng

Thông tin chạy web trên local
1. Domain chạy trên local: http://0.0.0.0:8184
2. Thông tin đăng nhập database: 
    a. host: 0.0.0.0
    b. Username: homestead
    c. Password: secret